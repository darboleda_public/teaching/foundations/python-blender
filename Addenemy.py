import bge
import random

cont = bge.logic.getCurrentController()
own = cont.owner
scene = bge.logic.getCurrentScene()

if own['cont'] > 1 and own['total']<9:    
    node = scene.objectsInactive['Armature.zombie']
    own.worldPosition = [random.randint(0,10),random.randint(20,60),1.6]
    life_time = 0
    obj =scene.addObject(node,own, life_time)
    own['cont'] = 0
    own['total'] += 1
