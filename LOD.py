from bge import logic
cont = logic.getCurrentController()
own = cont.owner

detail1 = 'Cube.002'
detail2 = 'Cube.001'
detail3 = 'Cube'

scene = logic.getCurrentScene()
objs = cont.sensors['LOD'].hitObjectList
rayDist = cont.sensors['LOD'].distance

for item in objs:
    if 'lod' in item:
        dist = own.getDistanceTo(item)
        # Define the different stages:
        if dist > rayDist-(rayDist*0.1):
            item.replaceMesh(detail3,1,1)
        elif dist > rayDist * 0.5:
            item.replaceMesh(detail2,1,1)
        else:
            item.replaceMesh(detail1,1,1)