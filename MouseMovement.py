import bge

def gameWindow():
    width = bge.render.getWindowWidth()
    height = bge.render.getWindowHeight() 
    return (width, height)

def mouseMove(gameScreen, controller, obj):
    mouse = controller.sensors["MouseMov"]
    width = gameScreen[0]
    height = gameScreen[1]
    x = width/2 - mouse.position[0]
    y = height/2 - mouse.position[1]
    
    if not 'mouseInit' in obj:
        obj['mouseInit'] = True
        x = 0
        y = 0
    
    if not mouse.positive:
        x = 0
        y = 0

    return (x, y)

def mouseSen(sensitivity, obj):
    if 'Adjust' in obj:
        if obj['Adjust'] < 0.0:
            obj['Adjust'] = 0.0        
        sensitivity = obj['Adjust'] * sensitivity
    return sensitivity

def mousePitch(invert, obj):
    if 'Invert'in obj:
        if obj['Invert'] == True:
            invert = -1
        else:
            invert = 1
    return invert

def mouseCap(capped, move, invert, obj):
    if 'Cap' in obj:			
        import mathutils
        if obj['Cap'] > 180:
            obj['Cap'] = 180
        if obj['Cap'] < 0:
            obj['Cap'] = 0

        camOrient = obj.localOrientation
        camZ = [camOrient[0][2], camOrient[1][2], camOrient[2][2]]
        vec1 = mathutils.Vector(camZ)
        camParent = obj.parent
        parentZ = [ 0.0, 0.0, 1.0]
        vec2 = mathutils.Vector(parentZ)
        rads = mathutils.Vector.angle(vec2, vec1)
        angle = rads * ( 180.00 / 3.14)
        capAngle = obj['Cap']
        moveY = move[1] * invert

        if (angle > (90 + capAngle/2) and moveY > 0)   or (angle < (90 - capAngle/2) and moveY < 0)  == True:
            capped = True
    return capped

def useMouseMov(controller, capped, move, invert, sensitivity):
    if capped == True:
        upDown = 0
    else:
        upDown = move[1] * sensitivity * invert

    lR = move[0] * sensitivity * invert
    act_LR = controller.actuators["LR"]
    act_UD = controller.actuators["UD"]
    act_LR.dRot = [ 0.0, 0.0, lR]
    act_LR.useLocalDRot = False    
    act_UD.dRot = [ upDown, 0.0, 0.0]
    act_UD.useLocalDRot = True
    controller.activate(act_LR)
    controller.activate(act_UD) 

def centerCursor(controller, gameScreen):
    width = gameScreen[0]
    height = gameScreen[1]
    mouse = controller.sensors["MouseMov"]
    pos = mouse.position
    if pos != [int(width/2), int(height/2)]:
        bge.render.setMousePosition(int(width/2), int(height/2))
    else:
        act_LR = controller.actuators["LR"]
        act_UD = controller.actuators["UD"]
        controller.deactivate(act_LR)
        controller.deactivate(act_UD)

Sensitivity =  0.0005
Invert = 1
Capped = False    
controller = bge.logic.getCurrentController()    
obj = controller.owner    
gameScreen = gameWindow()
move = mouseMove(gameScreen, controller, obj)
sensitivity =  mouseSen(Sensitivity, obj)
invert = mousePitch(Invert, obj)
capped = mouseCap(Capped, move, invert, obj)
useMouseMov(controller, capped, move, invert, sensitivity)
centerCursor(controller, gameScreen)
