import bpy
from math import sin, cos, radians
import numpy
import random

class Explotion:
    def __init__(self, initFrame, animDuration, speedMov, speedRot):
        self.initFrame = initFrame
        self.animDuration = animDuration
        self.speedMov = speedMov
        self.speedRot = speedRot
                    
    def setMovementVector(self):
        if self.posX <= self.posY:
            factor = self.posX/self.posY
            self.speedX = factor*self.speed
            self.speedY = self.speed
        else:
            factor = self.posY/self.posX
            self.speedY = factor*self.speed
            self.speedX = self.speed
        if (self.posY <0 and self.posX < 0):
            self.speedY *= -1
            self.speedX *= -1
            
        if self.posZ <= self.posY:
            factor = self.posZ/self.posY
            self.speedZ = factor*self.speed
        else:
            factor = self.posY/self.posZ
            self.speedZ = self.speed
        if (self.posZ < 0):
            self.speedZ *= -1
            
    def initObject(self, obj):
        self.speed = random.randint(1, self.speedMov)
        self.speedRot = random.randint(5, self.speedRot)
        current = numpy.array(obj.location)            
        self.posX = current[0]
        self.posY = current[1]
        self.posZ = current[2]
        self.setMovementVector()
            
    def animate(self, collectionName):
        for collection in bpy.data.collections:
            if collection.name == collectionName: 
                for obj in collection.objects:
                    self.initObject(obj)
                    frameNumber = self.initFrame                        
                    for j in range (0,self.animDuration,1):
                        obj.location = (self.posX, self.posY, self.posZ)
                        obj.rotation_euler = (radians(frameNumber*self.speedRot),radians(frameNumber*self.speedRot),radians(frameNumber))
                        obj.keyframe_insert(data_path="location", frame=frameNumber )
                        obj.keyframe_insert(data_path="rotation_euler", frame=frameNumber )
                        frameNumber +=1
                        self.posX += self.speedX
                        self.posY += self.speedY
                        self.posZ += self.speedZ

explotion = Explotion(2, 150, 7, 20)
explotion.animate("cubes") 
