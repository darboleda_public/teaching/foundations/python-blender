import bpy

speed = 3
frameInit = 1

speakDictionary =  {
  "A": "boca.a",
  "E": "boca.e",
  "I": "boca.i",
  "Y": "boca.i",
  "O": "boca.o",
  "U": "boca.u",
  " ": "boca.closed",
}

silentSpeakDictionary =  {
  "#": "",
}

def resetMouth(frameNumber):
    bpy.data.shape_keys["Key.002"].key_blocks["boca.a"].value = 0
    bpy.data.shape_keys["Key.002"].key_blocks["boca.e"].value = 0    
    bpy.data.shape_keys["Key.002"].key_blocks["boca.i"].value = 0
    bpy.data.shape_keys["Key.002"].key_blocks["boca.o"].value = 0
    bpy.data.shape_keys["Key.002"].key_blocks["boca.u"].value = 0
    bpy.data.shape_keys["Key.002"].key_blocks["boca.closed"].value = 0
    
    bpy.data.shape_keys["Key.002"].key_blocks["boca.a"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.002"].key_blocks["boca.e"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.002"].key_blocks["boca.i"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.002"].key_blocks["boca.o"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.002"].key_blocks["boca.u"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.002"].key_blocks["boca.closed"].keyframe_insert("value", frame = frameNumber)

    bpy.data.shape_keys["Key.003"].key_blocks["boca.a"].value = 0
    bpy.data.shape_keys["Key.003"].key_blocks["boca.e"].value = 0
    bpy.data.shape_keys["Key.003"].key_blocks["boca.i"].value = 0
    bpy.data.shape_keys["Key.003"].key_blocks["boca.o"].value = 0
    bpy.data.shape_keys["Key.003"].key_blocks["boca.u"].value = 0
    bpy.data.shape_keys["Key.003"].key_blocks["boca.closed"].value = 0
    
    bpy.data.shape_keys["Key.003"].key_blocks["boca.a"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.003"].key_blocks["boca.e"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.003"].key_blocks["boca.i"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.003"].key_blocks["boca.o"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.003"].key_blocks["boca.u"].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.003"].key_blocks["boca.closed"].keyframe_insert("value", frame = frameNumber)

def animMouth(vowel, frameNumber):
    resetMouth(frameNumber)
    bpy.data.shape_keys["Key.002"].key_blocks[speakDictionary[vowel]].value = 1
    bpy.data.shape_keys["Key.002"].key_blocks[speakDictionary[vowel]].keyframe_insert("value", frame = frameNumber)
    bpy.data.shape_keys["Key.003"].key_blocks[speakDictionary[vowel]].value = 1
    bpy.data.shape_keys["Key.003"].key_blocks[speakDictionary[vowel]].keyframe_insert("value", frame = frameNumber)

#message = "hola"
message = "Hola amig@s, no te olvides de suscribirte al canal y darle like!" #160-316

resetMouth(frameInit)
for c in message.upper():    
    if c in speakDictionary:
        animMouth(c,frameInit)
        frameInit = frameInit + speed
    if c in silentSpeakDictionary:
        frameInit = frameInit + speed

